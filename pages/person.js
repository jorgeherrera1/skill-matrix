import Page from '../layouts/main';
import LevelContainer from '../components/level-container';

export default () => (
  <Page>
    <header>
      <h1>Jorge Herrera</h1>
    </header>
    <main>
      <LevelContainer />
    </main>
  </Page>
);
