import Page from '../layouts/main';
import LevelContainer from '../components/level-container';

export default () => (
  <Page>
    <header>
      <h1>JavaScript</h1>
    </header>
    <main>
      <LevelContainer />
    </main>
  </Page>
);
