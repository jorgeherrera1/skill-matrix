import Chip from './ui/chip';

export default ({skill}) => (
  <Chip text={skill} />
);
